require 'httparty'

module GLMetadata
  GITLAB_METADATA_URL = 'https://gitlab.com/api/v4/metadata'.freeze

  class << self
    include HTTParty

    def fetch_version
      # Add in auth header data
      response = get(GITLAB_METADATA_URL)
      return nil unless response.success?

      data = response.parsed_response
      # This should work
      data['version']
    rescue StandardError => e
      puts "Error fetching version: #{e.message}"
      nil
    end
  end
end

# Usage would become something like ...
# version = GLMetadata.fetch_version
# puts "GitLab Version: #{version}" if version
#
# Sample return data from Metadata api endpoint ...
# {
#   "version": "15.2-pre",
#   "revision": "c401a659d0c",
#   "kas": {
#     "enabled": true,
#     "externalUrl": "grpc://gitlab.example.com:8150",
#     "version": "15.0.0"
#   },
#   "enterprise": true
# }
